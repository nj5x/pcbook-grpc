package serializer

import (
	"encoding/json"
	"io/ioutil"

	"google.golang.org/protobuf/proto"
)

func WriteProtobufToBinaryFile(message proto.Message, filename string) error {
	buf, err := proto.Marshal(message)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(filename, buf, 0644); err != nil {
		return err
	}
	return nil
}

func WriteToJsonFile(obj any, filename string) error {
	buf, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(filename, buf, 0644); err != nil {
		return err
	}
	return nil
}

func ReadProtobufFromBinaryFile(filename string, message proto.Message) error {
	buf,err:=ioutil.ReadFile(filename)
	if err!=nil {
		return err
	}
	return proto.Unmarshal(buf, message)
}
