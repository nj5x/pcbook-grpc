package serializer_test

import (
	"testing"

	"codeberg.org/nj5x/pcbook-grpc/pb"
	"codeberg.org/nj5x/pcbook-grpc/serializer"
	"codeberg.org/nj5x/pcbook-grpc/simple"
	"github.com/stretchr/testify/assert"
)

func TestFileSerializer(t *testing.T) {
	filename := "../tmp/laptop.data"
	// json_filename := "../tmp/laptop.json"
	laptop1 := simple.NewLaptop()

	// 230
	err := serializer.WriteProtobufToBinaryFile(laptop1, filename)
	assert.Nil(t, err)
	laptop1_json, err := serializer.ProtobufToJSON(laptop1)
	assert.Nil(t, err)
	// 755
	// err = serializer.WriteToJsonFile(laptop, json_filename)
	// assert.Nil(t, err)

	laptop2 := &pb.Laptop{}
	err = serializer.ReadProtobufFromBinaryFile(filename, laptop2)
	assert.Nil(t, err)

	laptop2_json, err := serializer.ProtobufToJSON(laptop2)
	assert.Nil(t, err)

	assert.Equal(t, laptop1_json, laptop2_json)
}
