.PHONY: gen
gen: ./proto/*.proto
	rm -rf ./pb && mkdir ./pb && \
	protoc -I ./proto --go_out=./pb --go-grpc_out=./pb ./proto/*.proto